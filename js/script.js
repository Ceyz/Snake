//Genere la map
function generateMap(){
	var td = "";
	var tr = [];

	for(var i = 0; i <= map; i++){
		td += "<td></td>";
	}

	for(var i = 0; i <= map; i++){
		tr.push('<tr>'+td+'</tr>');
	}

	$('.cadre').append('<table>'+tr.join('\n')+'</table>');
}

//Genere le snake
function generateSnake(){
	$('td').removeClass('head_snake body_snake');
	for(var cell in body_snake){
		$('tr').eq(body_snake[cell][0]).find('td').eq(body_snake[cell][1]).addClass('body_snake');
	}
	$('tr').eq(head_snake[0]).find('td').eq(head_snake[1]).addClass('head_snake');
}


//Actualise la position du snake
function move_snake(){
	var new_head = [];
	var size_snake = body_snake.length;

	if(direction == "right"){
		new_head = [head_snake[0],head_snake[1]+1];
	}
	else if(direction == "left"){
		new_head = [head_snake[0],head_snake[1]-1];
	}
	else if(direction == "up"){
		new_head = [head_snake[0]-1,head_snake[1]];
	}
	else if(direction == "down"){
		new_head = [head_snake[0]+1,head_snake[1]];
	}

	cell = $('tr').eq(new_head[0]).find('td').eq(new_head[1]);
	if(cell.hasClass('fruit')){
		body_snake.push([]);
		generateFruit();
		score += 100;
		if(score > best){
			best = score;
			localStorage.setItem('best', best);
		}
		$('#score span').html(''+score);
	}
	else if(cell.hasClass('body_snake')){
		gameOver();
	}
	else if(new_head[0] < 0 || new_head[1] < 0){
		gameOver();

	}
	else if(new_head[0] > map || new_head[1] > map){
		gameOver();
			
	}

	for(var i = size_snake-1;i>0;i--){
		body_snake[i] = body_snake[i-1];
	}

	body_snake[0] = head_snake = new_head;
	generateSnake();
}

// init game
function init(){
	reset();
	$('#gameOver').hide();
	$('#menu').hide();
	go = setInterval(move_snake,speed);
	generateMap();
	change_direction();
	generateFruit();
}

function change_direction(){
	var left = 37, up = 38, right = 39, down = 40;
	$('body').keydown(function(e){	
		if(e.keyCode == left){
			if(direction == "right"){
			}else{
				direction = "left";
			}
		}
		else if(e.keyCode == up){
			if(direction == "down"){
			}else{
				direction = "up";
			}
		}
		else if(e.keyCode == down){
			if(direction == "up"){
			}else{
				direction = "down";
			}
		}
		else if(e.keyCode == right){
			if(direction == "left"){

			}else{
				direction = "right";
			}
		}
	});
}

//Genere le fruit
function generateFruit(){
	$('td').removeClass('fruit');
	function verif(){
		fruit = [parseInt(Math.random() * map),parseInt(Math.random() * map)];
	}
	verif();

	if($('tr').eq(fruit[0]).find('td').eq(fruit[1]).hasClass('body_snake')){
		generateFruit();
	}else{
		$('tr').eq(fruit[0]).find('td').eq(fruit[1]).addClass('fruit');
	}
}

//Game over
function gameOver(){
	$('#gameOver').show('fast',function(){

		$(this).animate({top:200},'slow');
		$(this).animate({opacity: 0}, 'slow');
	});
	$('table').css('animation-play-state', 'running');

	setTimeout(function(){
		$('#gameOver').animate({opacity: 1}, 'slow');
		$('.hide').text('Nouvelle partie');
	}, 1350);

	setTimeout(function(){
		$('#menu').css('opacity', '0');
		$('#menu').show();
		$('#menu').animate({opacity: 1}, 1200);
	}, 2300);

	if(typeof go != 'undefined'){
		clearInterval(go);
	}
}


function menu(){
	$('.ul1 li').on({
		click: function(){
			var vitesse = $(this).index();
			var nbr = 150 - 25*vitesse;
			speed = nbr;
		}
	})

	$('.ul1 li').on({
		click: function(){
			$('.ul1 li').css('background-color', 'black');
			$(this).css('background-color', '#f97500');
		}
	})

		$('.ul2 li').on({
		click: function(){
			$('.ul2 li').css('background-color', 'black');
			$(this).css('background-color', '#f97500');
		}
	})

	$('.ul2 li').on({
		click: function(){
			var taille = $(this).index();
			var nbr = 10 + 10*taille;
			map = nbr;
			console.log(nbr);
		}
	})

	$('.start').on({
		click: function(){
			init();
		}
	})
}

function reset(){
	$('.cadre').html(null);
	body_snake = [[0,5], [0,4], [0,3]];
	head_snake = [0,5];
	direction = "right";
	score = 0;
}